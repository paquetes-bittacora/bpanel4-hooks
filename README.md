
# bPanel4 Hooks
Paquete para crear 'hooks' similares a los de WordPress en bPanel4.

## Uso

El uso de este paquete se hace a través de la clase `Bittacora\Bpanel4\HooksComponent\Services\Hooks`. En los ejemplos supongo que la clase se ha añadido usando inyección de dependencias, pero también es posible usar los métodos que comento a través de la fachada Bittacora\Bpanel4\HooksComponent\Facades\Hook o del alias Bp4Hook. Unos ejemplos en blade:

```html
<!-- A través de la fachada -->
{{ \Bittacora\Bpanel4\HooksComponent\Facades\Hook::execute('nombre-del-hook') }}

<!-- A través del alias -->
{{ Bp4Hook::execute('nombre-del-hook') }}
```

### Crear un hook

Para crear un hook solo tenemos que llamar al método `execute` de la clase `Hooks` en la línea de nuestro código en la que queramos que otros paquetes puedan hacer algo. Esto depende totalmente de lo que queramos que se pueda hacer, así que es recomendable documentar que este hook existe para que los demás lo sepan y puedan usarlo. No es necesario declarar nada en ningún `service provider` ni nada.

```php
$this->hooks->execute('display-additional-info', $argumento1, $argumento2, $argumentoN);
```

El primer parámetro es el nombre del hook, que puede ser lo que queramos. Esto se usará a la hora de registrar qué se ejecuta en el hook desde otros paquetes (ver más abajo). El resto de argumentos son los que se pasarán al código que se ejecutará, y podemos poner tantos como queramos y con los nombres que queramos, separándolos por comas. En el ejemplo hay 3, pero puede haber 1, ninguno, 5...

### Ejecutar código en un hook
Lo interesante de los hooks es registrar código que se ejecutará en un momento preestablecido, sin que el código que define el hook necesite saber nada del código que se ejecutará en ese punto, así que vamos a ver cómo ejecutaríamos código en el hook `display-additional-info` que hemos definido antes.

En el `service provider` del paquete desde el que queremos ejecutar algo en ese hook, hacemos una llamada como la siguiente: 

```php 
// $hooks es una instancia de Bittacora\Bpanel4\HooksComponent\Services\Hooks
$hooks->register('display-additional-info', [  
    $this->app->make(NuestraClase::class),  
  'metodoQueQueremosEjecutar',  
]);
```
El primer parámetro, `display-additional-info` es el nombre del hook que ya se ha definido (normalmente en otro paquete), y el segundo un `callable` que será lo que se ejecutará en el hook. En este caso está con la sintaxis tipo array, pero se podría pasar una función anónima, por ejemplo.

Ahora simplemente tenemos que crear el código que queremos que se ejecute:

```php 
<?php  
  
declare(strict_types=1);  
  
namespace Bittacora\Bpanel4\Modulo\Hooks;  
  
final class NuestraClase
{  
  public function handle(&$argumento1, &$argumento2, &$argumentoN): void  
  {  
        $argumento1[] = ['title' => 'Fila añadida desde el hook', 'value' => 'Probando 1234'];  
  }  
}
```

Es importante tener en cuenta que los argumentos se pasan por referencia, para así poder modificarlos. En el ejemplo simplemente se añade una entrada al array del primer parámetro. Esta modificación estaría disponible en la línea siguiente a la llamada a `execute` del primer ejemplo.