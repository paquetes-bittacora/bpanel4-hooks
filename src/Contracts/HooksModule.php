<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\HooksComponent\Contracts;

interface HooksModule
{
    public function register(string $hookName, callable $callable, int $priority = 0): void;
}