<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\HooksComponent\Facades;

use Bittacora\Bpanel4\HooksComponent\Services\Hooks;
use Illuminate\Support\Facades\Facade;

/**
 * @method static void register(string $hookName, callable $callable, int $priority = 0) Registra un hook
 * @method static void execute(string $hookName, &...$args) Ejecuta un hook
 */
final class Hook extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return Hooks::class;
    }
}