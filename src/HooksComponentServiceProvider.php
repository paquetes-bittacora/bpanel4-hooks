<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\HooksComponent;

use Bittacora\Bpanel4\HooksComponent\Contracts\HooksModule;
use Bittacora\Bpanel4\HooksComponent\Services\Hooks;
use Illuminate\Support\ServiceProvider;

final class HooksComponentServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $hooks = $this->app->make(Hooks::class);
        $this->app->instance(Hooks::class, $hooks);
        $this->app->bind(HooksModule::class, Hooks::class);
    }
}
