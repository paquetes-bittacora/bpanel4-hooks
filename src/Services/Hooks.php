<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\HooksComponent\Services;

use Bittacora\Bpanel4\HooksComponent\Contracts\HooksModule;

final class Hooks implements HooksModule
{
    /**
     * @var array<string, array{callable: callable, priority: int}>
     */
    private array $hooks = [];

    public function register(string $hookName, callable $callable, int $priority = 0): void
    {
        if (!isset($this->hooks[$hookName])) {
            $this->hooks[$hookName] = [];
        }

        $this->hooks[$hookName][] = [
            'callable' => $callable,
            'priority' => $priority,
        ];
    }

    public function execute(string $hookName, &...$args): void
    {
        if (!isset($this->hooks[$hookName])) {
            return;
        }

        usort($this->hooks[$hookName], static function (array $a, array $b): int {
            return $a['priority'] <=> $b['priority'];
        });

        foreach ($this->hooks[$hookName] as $callable) {
            $callable['callable'](...$args);
        }
    }
}
