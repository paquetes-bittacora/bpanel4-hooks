<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\HooksComponent\Tests\Unit;

use Bittacora\Bpanel4\HooksComponent\Services\Hooks;
use Tests\TestCase;

final class HooksTest extends TestCase
{
    private Hooks $hooks;

    protected function setUp(): void
    {
        parent::setUp();
        $this->hooks = new Hooks();
    }

    public function testEjecutaElCodigoAsociadoAUnHook(): void
    {
        $this->hooks->register('test-hook', static function (string &$param) {
            $param = 'modificado';
        });

        $exampleParam = '';
        $this->hooks->execute('test-hook', $exampleParam);

        self::assertEquals('modificado', $exampleParam);
    }
}
